const CONFIG = require('../env_config');
const puppeteer = require('puppeteer');
const lineReader = require('promise-readline');
const mongoClient = require('mongodb').MongoClient;
const Binary = require('mongodb').Binary;
const urlfilter = require('../urlfilter');
const sqlite = require('./sqlite_util.js');
const spawn = require('child_process').spawn;
const cookiejar = require('../cookiejar');
const urlextract = require('../urlextract.js');

const sleep = (time) => new Promise(resolve => setTimeout(resolve, time));
function getRandomInt(max) { return Math.floor(Math.random() * Math.floor(max)); }

class QueueItem {

    // @param {UrlMeta} meta
    constructor(meta) {
        this.meta = meta;
        this.attempts = 0;
        this.page = undefined;
        this.blockCount = 0;
        this.shortUrl = meta.url.substring(0, 50);
    }
}

process.on('warning', e => console.warn(e.stack));

const maxTabs = CONFIG.maxTabs;

var pageCount = 0;

let mongo = {
    url: CONFIG.mongoUrl,
    databaseName: CONFIG.mongoDb,
    client: undefined,
    database: undefined
}

var response_collection = CONFIG.mongoCollection;

async function initMongo(config) {

    config.client = await mongoClient.connect( config.url, { useNewUrlParser: true } );
    config.database = mongo.client.db(config.databaseName);
}

function closeMongo() {

    mongo.client.close();    
}

/*
 * Function acknowledgeDuplicate(url) updates a count of duplicate URLs
 */
async function logUrlFailure(meta) {

    sqlite.addBinaryMd5(meta.urlMd5.buffer);
}


/*
 * Function md5UrlExists(strippedUrl) checks if the given URL has already been processed.
 * Returns an object with boolean urlExists, and the BSON MD5 object.
 */
async function md5UrlExists(md5HexString) {

    const md5Bytes = Buffer.from(md5HexString, "hex");
    const md5Binary = new Binary(md5Bytes, Binary.SUBTYPE_MD5);

    // Check for the document by url or meta.url
    const col = mongo.database.collection(response_collection);

    const hasUrl = await col.find( { $or: [
        { "urlMd5": md5Binary },
        { "meta.urlMd5": md5Binary } 
    ] }, { limit: 1 }).hasNext();

    return { urlInDatabase: hasUrl, md5BSON: md5Binary };
}



/*
 * Function writeMongo(url, strippedUrl, content, blockCount, sourceMeta)
 * writes the given data to the database. The source URL refers to the URL fed
 * into the process which might be different (e.g. redirects) from the URL of
 * the resulting page.
 */
async function writeMongo(url, md5BSON, title, content, links, meta) {

    // Insert a document
    const col = mongo.database.collection(response_collection);
    
    let mongoId;

    const document = {
        url: url,
        urlMd5: md5BSON,
        title: title,
        content: content,
        textlength: content.length,
        meta: meta 
    };

    // We never need the original URL, only page.url().
    delete document.meta.url;

    // Also store the meta URL's MD5 if not same as page.url's, and
    // rename in line with the MongoDb collection.
    if( ! md5BSON.buffer.equals(meta.md5Bin) ) {
        document.meta.urlMd5 = new Binary(document.meta.md5Bin, Binary.SUBTYPE_MD5);
    }
    delete document.meta.md5Bin;

        
    if(links.length > 0) {
        document.links = links;
    }

    const result = await col.insertOne(document);

    console.log('STORE:', result.insertedId, ' for ', url.substring(0,50));
    mongoId = result.insertedId;

    return mongoId;
}

/*
 * Function extractContent will take the HTML content of the given page
 * and process it for text extraction. The resulting text will be returned
 * as a single string.
 *
 * The current implementation spawns Python justext to extract the text.
 */
async function extractContent(contentStr) {

    const child = spawn('/usr/bin/python', ['-m', 'justext', '-s',
        'English'
    ]);

    child.stdout.setEncoding('utf8');

    let contentBlocks = [];

    /*
     * Install a handler for 'data' events, which represent data being
     * written to stdout. This can be called multiple times.
     */
    child.stdout.on('data', (contentChunk) => {

        contentBlocks.push(contentChunk);
    });

    let promiseContent = new Promise( (resolve, reject) => {

        child.stdout.on('end', () => {

            const content = contentBlocks.join('');

            resolve(content);
        });
    });

    child.stdin.write(contentStr);
    child.stdin.end();
    
    return promiseContent;
}

let running = false;
let periodicTimer;

let queue = [];

/**
 * Reads from stdin and extracts viable source URLs. Any that match 'shouldDiscard' or
 * 'urlInDatabase' are skipped.
 *
 * The QueueItem
 */
async function fillQueueFromStdin() {

    /*
     * Main processing entry point, runs a loop every second, attempting
     * to process the next line (URL) from stdin. This attempts to run
     * a limited number of simultaneous tabs without under-utilising resources.
     */

    await initMongo(mongo).catch( () => {
        throw "Failed to create the mongo client.";
    });


    let lr = lineReader(process.stdin);
    let lineCount = 0;


    let line;
    while( (line = await lr.readLine()) != null) {

        const bits = line.match(/[CS](\d+):(\d+):(.+)$/);
        if(bits !== null) {

            let url = bits[3];
            let urlInfo = urlfilter.getUrlInfo(url);

            // Skip anything any meaningless or ignorable URL.
            if( url.length < 12 || urlInfo.shouldDiscard)
                continue;

            let md5Bin = Buffer.from( urlInfo.md5Url, "hex" );

            // Skip any URL we've seen fail in the past
            if(sqlite.urlExists(md5Bin))
                continue;

            let { urlInDatabase, md5BSON } = await md5UrlExists(urlInfo.md5Url);

            /*
             * @typedef {Object} UrlMeta
             *   @property {string} url
             *   @property {Buffer} md5Bin
             */
            let meta = {
                url: url,
                md5Bin: md5Bin,
                hnId: bits[2],
                score: parseInt(bits[1], 10),
                blockCount: 0
            };

            if(urlInDatabase == false) {

                console.log("Pushing Meta for", line);
                queue.push( new QueueItem(meta) );
                
            } else {
                console.log('Skipping URL ', url.substring(0,50), 'because urlInDatabase');
            }
        }
    }

    console.log("Queueing complete. Items: ", queue.length);

    queue.reverse();

    return queue.length;
}

async function runQueueThroughPuppeteer() {

    /*
     * Launch the browser synchronously.
     */
    const browser = await puppeteer.launch({

        headless: false,
        timeout: 20000,
        dumpio: false,
        ignoreHTTPSErrors: true,

        args: [
            '--no-sandbox',
            '--disable-setuid-sandbox',
//            '--disable-dev-shm-usage',
            '--disable-extensions-except=' + CONFIG.uBlockPath,
            '--load-extension=' + CONFIG.uBlockPath,
            '--proxy-server=93.119.178.129:8118'
        ]
    });

    console.log("Browser User-Agent:", await browser.userAgent());
    console.log("Sleeping to allow uBlock to compile fiters");
    await sleep(5000);

    /**
     * Function runQueue() invokes doPage() for the next available QueueItem.
     * If the outcome is an exception, then runQueue decides whether or not it
     * should be requeued or discarded.
     */
    async function runQueue() {

        if(pageCount < maxTabs) {

            let queueItem = queue.pop();

            queueItem.attempts += 1;

            if(queueItem.attempts > 3) {
                // TODO: Discard this meta
            }

            pageCount += 1;
            await doPage(queueItem)

            .then( (page) => {
                console.log("Decrementing page count of", pageCount, "(OK)");
                pageCount -= 1;
            })
            .catch( async (err) => {
                console.log("Decrementing page count of", pageCount, "(FAIL)");
                pageCount -= 1;
                await killPage(queueItem)
                .catch( console.log );
                console.log('CAUGHT',  err.message, 'from doPage()');
                if(err.hardFail || queueItem.attempts === 3) {
                    console.log("Hard fail for", queueItem.shortUrl);
                    sqlite.addBinaryMd5(queueItem.meta.md5Bin);
                } else {
                    // This item is a candidate for a retry
                    console.log("REQUEUE because", err.message + ":", queueItem.shortUrl);
                    queue.unshift(queueItem);
                }

            });
        }
    }


    /*
     * Function doPage(url) creates a new tab (page) and installs a handler for
     * the 'domcontentloaded' event that extracts and processes the page content
     * inserting it into the database for indexing.
     *
     * It also installs a handler the page 'load' event, which simply initiates
     * the delayed close (using endPage()), and a handler for 'requestfailed'.
     * 
     * The given URL is finally invoked with a page.goto(), run synchronously.
     */
    async function doPage(queueItem) {

        let meta = queueItem.meta;
        let url = meta.url;

        console.log('doPage[', pageCount, '] for', queueItem.shortUrl, 'QR',queue.length);

        await sleep(500 + getRandomInt(1500));

        const page = await browser.newPage();

        /**
         * For sites that require consent to view, e.g. for GDPR, it's
         * sometimes possible to automatically provide a consent cookie,
         * so this code checks the URL and sets cookies if present.
         */
        const cookies = cookiejar.cookiesForUrl(url);

        if(cookies) {
            await page.setCookie(...cookies);
        }

        await page._client.send('Page.setDownloadBehavior', {
                    behavior: 'deny'
        });

        queueItem.page = page;
        meta.blockCount = 0;

        /* Handler for requestfailed uBlock response */
        page.on('requestfailed', request => {

            if (request.failure() != null) {

                var errorText = request.failure().errorText;
                if (errorText === 'net::ERR_BLOCKED_BY_CLIENT') {
                    //console.log('EVENT: requestFailed', errorText);
                    meta.blockCount++;
                }
            }
        });

        /* Automatically dismiss any dialog that might appear */
        page.on('dialog', dialog => dialog.dismiss());

        // Initiate page loading early, which means redirects are followed
        // before the on.domcontentloaded and on.load event handlers are
        // installed. Otherwise, the handlers can fire on transient pages,
        // such as those with meta-redirects.

        let response;
        //let timeout;

        try {

            console.log("GOTO:", queueItem.shortUrl);

            // Set timebomb for the page, 30 seconds total
            //timeout = setTimeout(async function abandonPage() {
            //
            //    if(typeof queueItem.page !== 'undefined') {
            //        await killPage(queueItem)
            //        .catch(err => console.log);
            //    }
            //}, 30000);

            response = await page.goto(url, {
                timeout: 10000,
                waitUntil: 'load'
            });
        }
        catch(err) {

            let message = typeof(err) == 'object' ? err.message : err;

            // If we got a navigation timeout on page.goto() it may have been
            // because there's too much content to load quickly. Therefore, try
            // to process it. We'll get back here with a different exception if
            // the problem was elsewhere.

            if(/^Navigation Timeout/.test(message)) {

                console.log("doPage() goto() hit Navigation Timeout. Allow to continue.");

            } else if(/^net::ERR_(NAME_NOT_RESOLVED|CONNECTION_REFUSED)/.test(message)) {

                throw( { hardFail: true, message: message } );

            } else {
                //
                // message == 'net::ERR_NETWORK_CHANGED' - maybe others?
                throw( { hardFail: false, message: message } );
            }
        }

        if(response && response.status() == 404) {
            throw({ hardFail: true, message: "Not found: 404"});
        }

        // Disable the timebomb - not yet! check where the code is when browser is 'Waiting...'
        // clearTimeout(timeout);

        // Allow 3 seconds after load for content
        await sleep(3000);

        await processPage(queueItem)
        .catch( (err) => {
            if(/^Execution context was destroyed/.test(err.message)) {
                // Usually page.content() fails because the page navigated away.
                console.dir(response)
                throw({ hardFail: false, message: "Execution Context Destroyed"});
            }
            console.log("processPage() threw us an exception", err.message);
            throw(err);
        });

        return page;

    } // End doPage(url) - nag: this function is too long!



    /**
     * Process the given page, extracting content then collecting
     * stats to store.
     */
    async function processPage(queueItem) {

        let page = queueItem.page;

        const contentStr = await page.content();

        const content = await extractContent(contentStr);

        // Perhaps we want to check for 404 etc. here.
        if(content.length < 300) {
            throw({ hardFail: true, message: "Discard: no content"});
        }

        // Allow a further second or three for ad-blocks.
        await sleep(1000 + getRandomInt(2000));

        // Extract URLs from page and filter.
        const hrefs = await urlextract.extractHrefs(contentStr);

        // @type {UrlMapping[]} 
        let urls = [];

        for(let url of hrefs) {

            const urlInfo = urlfilter.getUrlInfo(url);

            if(urlInfo.shouldDiscard === false) {

                const existing = await md5UrlExists(urlInfo.md5Url);

                if(existing.urlInDatabase === false) {
                   // @type {UrlMapping}
                   const obj = {
                       srcMd5:  queueItem.meta.md5Bin,
                       destMd5: existing.md5BSON.buffer,
                       srcUrl:  queueItem.meta.url,
                       destUrl: url
                   }
                   urls.push(obj);
                }
            }
        }
        sqlite.addUrlMappings(urls);
        console.log("HREFs for", queueItem.meta.url);
        console.log('Retained', urls.length, 'links');
        queueItem.contentText = content;

        await endPage(queueItem);
    }


    /**
     * Unconditionally close the given page and mark the object as finished.
     */
    async function killPage(queueItem) {

        let page = queueItem.page;

        if(typeof page !== 'undefined') {
            
            page.removeAllListeners('requestfailed');

            await page.close() .catch( async (err) => {
                // Failing to close a tab indicates that something is wrong with
                // the browser process. It's safer to abandon with an error code.
                // In future, this may be a Puppeteer restart instead.
               
                console.log('killPage failed to close:', queueItem.shortUrl);
                console.log('This suggests a problem. Ditching with return code -1');

                await browser.close()
                .catch( (err) => {
                    console.log('Failed to close the browser');
                });
                process.exit(1);
            });
        }
    }


    /*
     * Function endPage() waits, then stores the page content, unless
     * the effective URL is to be discarded.
     */
    async function endPage(queueItem) {

        let page = queueItem.page;

        // Allow a delay beyond the page-load event to count further blocks
        // then close the page.
        await sleep(2000 + getRandomInt(1000));

        const url = page.url();
        const urlInfo = urlfilter.getUrlInfo( url );

        if(urlInfo.shouldDiscard == false) {

            const pageTitle = await page.title()
            .catch( err => {
                console.log(err);
                throw({ hardFail: false, message: "Failed on page.title()"});
            });

            const contentText = queueItem.contentText;

            let {urlInDatabase, md5BSON} = await md5UrlExists(urlInfo.md5Url);

            /**
             * Write the database document for this page if the URL
             * from page.url() is relevant and doesn't already exist.
             */
            if(urlInDatabase == false ) {
                    
                await writeMongo(
                    url, md5BSON, pageTitle, contentText, [],
                    queueItem.meta
                );
            } else {
                // If we're here and url _is_ in Database, then the source
                // URL should be added to url_failure. This can happen when
                // multiple and differing URLs lead to the same page.
                sqlite.addBinaryMd5(queueItem.meta.md5Bin);
            }
        } else {
            //console.log("HARD:p.url()", url.substring(0,25), "shouldDiscard", urlInfo.shouldDiscard );
            //sqlite.addBinaryMd5(queueItem.meta.md5Bin);
            throw( { 
                hardFail: true,
                message: url.substring(0,25) + " shouldDiscard:" + urlInfo.shouldDiscard
            });
        }

        await page.close()
        .catch((err) => {
            console.log('Failed closing page for: ', url)
        });
    }


    /**
     * Process the queue until all items have been processed. This code isn't
     * entirely correct, since an item can currently be requeued after the queue
     * length has reached zero.
     */
    while(queue.length > 0) {

        //console.log("Running queue");
        runQueue();
        await sleep(500);
    }

    /**
     * Begin the shutdown procedure, which waits for all tabs to be closed before
     * shutting the broser instance.
     */
    let countdown = 90;
    periodicTimer = setInterval( async () => {

        // There will always be the initial tab remaining.
        let tabsRemaining = await browser.pages();

        console.log('Closing browser in', (countdown * 2), 'seconds with', 
                       tabsRemaining.length, 'tabs remaining. pageCount', pageCount);
        countdown = countdown - 1;

        if(queue.length > 0)
            await runQueue();
        else {
            if(countdown == 0 || (pageCount == 0 && tabsRemaining.length == 1)) {
                clearInterval(periodicTimer);
                console.log("Closing browser instance");
                browser.close();
                closeMongo();
            }
        }
    }, 2000);

} /* End runQueueThroughPuppeteer() */


/*
 * Main entry point. First fill the queue with viable URLs, then
 * process them through Puppeteer.
 */
let queueSize = fillQueueFromStdin()
.then( (queueSize) => {
    if(queueSize > 0) {
        try {
            runQueueThroughPuppeteer()
            .then( () => {
                console.log("run() complete. Exit when pages are done.");
            })
            .catch( (err) => {
                console.log("runQueueThroughPuppeteer() catch()", err);
            });
        }
        catch(err) {
            console.log("fillQueueFromStdin() catch()", err);
        }
    } else {
        console.log("Queue is empty, exiting.");
        process.exit(0);
    }
})
.catch( (err) => {
    console.log("Failed to fill queue from stdin", err);
    process.exit(-1);
})

