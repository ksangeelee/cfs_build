const sqlite = require('./sqlite_util.js');

let url1 = 'abcdef01';
let url2 = 'bacdef02';

let urls = []

for(let x=0; x < 200; x++) {

    const r = Math.floor(Math.random()*1000000);
    const str = r.toString();

    let srcMd5 = Buffer.from(url1 + str, 'hex');
    let destMd5 = Buffer.from(url2 + str, 'hex');
    let srcUrl = 'https://' + url1 + str;
    let destUrl = 'https://'+ url2 + str;

    //console.log(srcMd5, srcMd5.length)
    //console.log(destMd5, destMd5.length)
    const obj = {srcMd5, destMd5, srcUrl, destUrl};
    urls.push(obj)
}
sqlite.addUrlMappings(urls);
