module.exports = {
    addBinaryMd5,
    urlExists,
    addUrlMappings
}

const betterSqlite3 = require('better-sqlite3');

const db = new betterSqlite3('cfs_urls.sqlite3');

db.exec(
    "create table if not exists url_failures" + 
    " (urlMd5 blob primary key) without rowid"
);

db.exec(
    "create table if not exists out_links (" + 
    " srcMd5 blob, destMd5 blob," +
    "srcDomain varchar, destUrl varchar" +
    ", primary key(srcMd5, destMd5)) without rowid"
);

// Statements for url_failures queries.
const s_fails_sel = db.prepare('select * from url_failures where urlMd5 = ?');
const s_fails_ins = db.prepare('insert into url_failures (urlMd5) VALUES (?)');

/**
 * Adds a binary MD5 buffer to the url_failures table.
 * @param {Buffer} buf - the MD5 buffer of 16 bytes.
 */
function addBinaryMd5(buf) {

    try {
        const info = s_fails_ins.run(buf);
    }
    catch(err) {
        console.log("addBinaryMd5() failed:", err.message);
    }
}

/**
 * Tests if the given MD5 exists in the url_failure table.
 * @param {Buffer} binaryMd5 - the MD5 buffer to test for.
 * @returns {boolean}
 */
function urlExists(binaryMd5) {

    const row = s_fails_sel.get(binaryMd5);

    if(row) {
       return true; 
    }
    return false;
}

// Statements for outbound  queries.
const s_links_ins = db.prepare('insert or ignore into out_links(srcMd5,destMd5,srcDomain,destUrl)' +
    ' VALUES (?,?,?,?)');
const begin = db.prepare('begin');
const commit = db.prepare('commit');
const rollback = db.prepare('rollback');
/**
 * Add entries to the out_links to record the URLs contained within
 * the source page. This will act as a secondary source to further
 * index the web.
 * 
 * @typedef {Object} UrlMapping
 *   @property {Buffer} srcMd5  - source page URL MD5
 *   @property {Buffer} destMd5 - outbound link URL MD5
 *   @property {string} srcDomain  - source URL host/domain
 *   @property {string} destUrl - link URL
 *
 * @param {UrlMapping[]} urls - array of mapping objects.
 */
function addUrlMappings(urls) {
    // srcMd5, destMd5, srcDomain, destUrl
    try {
        begin.run();
        for(let url of urls) {
            s_links_ins.run( url.srcMd5, url.destMd5, url.srcDomain, url.destUrl);
        }
        commit.run();
    }
    catch(err) {
        console.log("addUrlMappings() failed:", err);
        rollback.run()
    }
}

