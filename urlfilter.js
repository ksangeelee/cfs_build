const crypto = require('crypto');

module.exports = {
    remapUrl,
    stripUrl,
    shouldIgnore,
    getUrlInfo
};

// Regex to remap Wikipedia
const mobileRemapRegEx = /^(https?:\/\/.{2})\.m(\.wikipedia.org)/;

// Regex to help remove page anchors
const pageAnchorRegEx = /#[^!].*$|#$/;

// Regex identifies URLs to ignore based on extension
const extensionsRegex = /https?.+\.(pdf|jpeg|jpg|zip|xls|xml|doc|gif|png|ico|css|woff2|js|svg)(\?.*)?$/i;

// Regex identifies URLs to ignore based on host/path
const hostsRegex = /https?:\/\/.*(localhost[:/]|imgur\.com\/|youtube.com\/(watch|embed|playlist|channel)|google\.com\/(maps|ngrams|search)|goo\.gl\/|flic\.kr\/|srf\.ch\/play|gstatic\.com\/images|vimeo\.com\/|youtu\.be\/|itunes\.apple\.com\/|twitter\.com\/|www.facebook.com\/|example\.com|github\..+\/blob\/|latimes\.com\/|\.washingtonpost\.com\/|slate\.com\/gdpr\?|pastebin\.com\/|chicagotribune\.com\/|guce\.oath\.com\/|\.wsj\.com\/)/


/**
 * Given a URL, return details of how the URL should be treated by the
 * system. Includes the stripped URL string and its MD5 hex string.
 */
function getUrlInfo(url) {

    let remappedUrl = remapUrl( url );
    let shouldDiscard = shouldIgnore(remappedUrl);

    if(shouldDiscard)
        return {shouldDiscard, remappedUrl};
    
    let strippedUrl = stripUrl(remappedUrl);
    let md5Url = crypto.createHash('md5').update(strippedUrl).digest("hex");

    return {shouldDiscard, remappedUrl, strippedUrl, md5Url};
}


/*
 * Returns a stripped version of the given URL, removing all non
 * alphabetic and non numeric characters.
 */
function stripUrl(url) {

    let urlStripped = url;
    if(url != null)
        urlStripped = url.replace(/(^https?|[^a-zA-Z0-9])/g, '');

    return urlStripped;
}


/*
 * Returns a remapped version of a URL, such as mapping en.m.wikipedia
 * to en.wikipedia, or the original URL if no remapping is needed.
 */
function remapUrl(url) {

    // We remove page anchors to avoid duplicate URLs. Allow page anchors
    // starting '#!' which usually carry an Id
    if( pageAnchorRegEx.test(url) )
        url = url.replace(pageAnchorRegEx, '');

    // Remap URL
    if( mobileRemapRegEx.test(url) )
        return url.replace( mobileRemapRegEx, '$1$2' );
    else
        return url;
}

/*
 * Given a URL, determine if the content it refers to should
 * be ignored. Returns false if valid, or a positive integer
 * if it should be ignored. The returned integer defines the
 * reason for ignoring.
 */
function shouldIgnore(url) {
    
    let matches = url.match( extensionsRegex );

    if(matches !== null)
        return "true: file extension";

    matches = url.match(hostsRegex);

    if(matches !== null)
        return "true: host/path";

    if(url.startsWith('http') === false)
        return "true: unsupported URL";

    return false;
}




