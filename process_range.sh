#!/bin/bash

if [ "$1" == "" ]; then
    echo "Usage: $0 <start idx>"
    exit
fi

HN_START=$1
HN_NUM_ITEMS=100000

SOURCE_LOG=urls_${HN_START}_${HN_NUM_ITEMS}.txt

echo "Fetching ${HN_NUM_ITEMS} items starting from  ${HN_START}"
node fetch_range.js ${HN_START} ${HN_NUM_ITEMS} >${SOURCE_LOG}
echo "Finished the batch started from ${HN_START}"

cd pupp

mv ./split_*.?? ./archive/ 2>/dev/null
mv ./split_*.log ./archive/ 2>/dev/null
mv ./split_*.err ./archive/ 2>/dev/null

cat ../${SOURCE_LOG} | shuf | split -d -l 1000 - split_${HN_START}.

for f in ./split_${HN_START}.??; do
    echo "Processing file ${f}"
    cat ${f}|node process_urls.js >${f}.log 2>${f}.err
done

