module.exports = {
    cookiesForUrl
};

const regExSlate = /^https?:\/\/.*slate\.com\//;

const cookiesSlate = [
    { 'domain': '.slate.com', 'name': 'GDPR_consent', 'value':'1' }
];

/**
 * For the given URL, return an array of cookies that can be set
 * prior to making a request.
 *
 * @return {array} Array of cookie Objects.
 */
function cookiesForUrl(url) {

    if(regExSlate.test(url))
        return cookiesSlate;

    return null;
}
