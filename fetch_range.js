/*
 * Iterate Hacker News item ids to extract stories and comments for any
 * URLs.
 */

const firebase = require('firebase');
const http = require('http');
const https = require('https');
const extractor = require('./urlextract');
const urlfilter = require('./urlfilter');

const Entities = require('html-entities').AllHtmlEntities;
const entities = new Entities();


/**
 * Given a Firebase DataSnapshot, extract the required values, along
 * with any URLs when the snapshot type is a 'comment'.
 */
function processHnItem(snapshot) {

    var snap = snapshot.val();

    //console.log(snap);

    var urls = [];

    if (snap !== null && snap.dead != true) {

        if (snap.type === 'comment') { // Comment

            extractor.extractHrefs(snap.text) // also dereferences
            .then( (hrefs) => {
              if (hrefs.length > 0) {
                for (var i = 0; i < hrefs.length; i++) {
                    let url = urlfilter.remapUrl( hrefs[i] );
                    const shouldIgnore = urlfilter.shouldIgnore(url);
                    if( shouldIgnore == false)
                        urls.push('C' + i + ':' + snap.id + ':' + url);
                    else
                        console.error("Discarding:", shouldIgnore, hrefs[i]);
                }
              }
	        });

        } else if (snap.type === 'story') { // Story

            if (snap.url != null && snap.url.length > 9) {
                extractor.dereferenceRedirectors(snap.url)
                .then(url => {
                    let remappedUrl = urlfilter.remapUrl(url)
                    if(urlfilter.shouldIgnore(remappedUrl) == false)
                        urls.push('S' + snap.score + ':' + 
                            snap.id + ':' + remappedUrl);
                    else
                        console.error("Discarding: ", url);
                })
            }

        } else { // Anything else...

            console.error('Unhandled: ' + snap.id + ' ' + snap.type);
        }
    }
    return urls;
}


/**
 * Fetch URLs from a batch of HN articles asynchronously, from startId working
 * back count items.
 */
function fetchSome(startId, count) {

    var promises = [];

    for (var offset = 0; offset < count; offset++) {

        let hnItem = ref.child('item/' + (startId - offset));

        promises.push(hnItem.once('value').then(processHnItem));
    }

    return Promise.all(promises);
}

/** 
 * Fetch all URLs found in items from startId, working backwards count items.
 * Works synchronously in batches.
 */
async function fetchAll(startId, count) {

    const batchSize = 500;
    var batchCount = Math.trunc(count / batchSize);
    var remainder = count % batchSize;
    var values = [];

    console.error('batchCount = ' + batchCount + ' and remainder = ' + remainder);

    for (var offset = 0; offset < batchCount; offset++) {
        let urls = await fetchSome(startId - (offset * batchSize), batchSize);
        if (urls.length > 0)
            values.push(urls);
    }
    if (remainder > 0) {
        let urls = await fetchSome(startId - (batchCount * batchSize), remainder);
        if (urls.length > 0)
            values.push(urls);
    }

    return values;
}


/*
 * Main entry point
 */

if (process.argv.length <= 3) {

    console.error('Usage: ' + process.argv[1] + ' <id> <count>');
    process.exitCode = 1;

} else {

    var app = firebase.initializeApp({
        databaseURL: 'https://hacker-news.firebaseio.com/'
    });
    var db = app.database();
    var ref = db.ref('/v0');

    var startId = process.argv[2],
        count = process.argv[3];

    fetchAll(startId, count)
    .then( function dumpAllReturned(values) {

        values.forEach((element) => {
            element.forEach((item) => {
                if (item.length > 0) {
                    for (let url of item) {
                        console.log(url);
                    }
                    //console.log('item = ' + typeof(item) + ': ' + item)
                }
            });


        });

        app.delete().then(function() {
            console.error('delete app instance - all done')
        });
    });
    console.error('Jumping back to ground.');
}
