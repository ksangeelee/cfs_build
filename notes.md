Notes on Processing URLs
========================

runQueue() -> doPage() -> endPage() -> writeMongo()



other outcomes

 -> requeuePage()
 -> discardPage()

requeue up to 3 times before discarding
discard marks a URL (source or both source & effective) as failed.

Naming convention:
                                    md5Url: hex_string,
                                    md5Bin: Buffer,
                                    md5BSON: Mongo object.
    let meta = {
        url: url,
        md5Bin: md5Bin,
        hnId: bits[2],
        score: parseInt(bits[1], 10),
        blockCount: 0
    };

    class QueueItem {

        constructor(meta) {

            this.meta = meta;
            this.attempts = 0;
            this.page = undefined;
        }
    }

    urlfilter.geturlInfo(): returns {shouldDiscard, remappedUrl, strippedUrl, md5Url};
        where md5Url is a string of hex.

CHECK: https://guce.oath.com/collectConsent?bra


// URL yields content
URL1 -> Content1

// URL yields redirect to content
URL1 -> URL2 -> Content1

// URL yields different content on each request
URL1 -> Content1, URL1 -> Content2

// URL yields different redirects to same content
URL1 -> URL2 -> Content1, URL1 -> URL3 -> Content1

// URL yields different redirects to different content
URL1 -> URL2 -> Content1, URL1 -> URL3 -> Content2
