let assert = require('assert');
let f = require('./urlfilter.js');


let strippedUrl, expectedUrl;

/*******************************************************
 * Test stripUrl(undefined)
 ******************************************************/
let urlUndefined = undefined;
expectedUrl = undefined;
console.log("stripUrl() Test 1", urlUndefined);
try {
    strippedUrl = f.stripUrl(urlUndefined);
    assert.equal(expectedUrl, strippedUrl);
    console.log("Passed");
}
catch(assertionError) {
    console.log("FAILED with", assertionError.message);
}


/*******************************************************
 * Test stripUrl(a:b/c)
 ******************************************************/
let url1 = 'a:b/c';
strippedUrl = f.stripUrl(url1);
expectedUrl = 'abc';
console.log("stripUrl() Test 2", url1);
try {
    assert.equal(expectedUrl, strippedUrl);
    console.log("Passed");
}
catch(assertionError) {
    console.log("FAILED with", assertionError.message);
}


/*******************************************************
 * Test stripUrl(http(s)://en.wikipedia.com/SomeTes...)
 ******************************************************/
let url2 = '://en.wikipedia.com/SomeTest_URL-?q=556&r=72';
expectedUrl = 'enwikipediacomSomeTestURLq556r72';
console.log("stripUrl() Test 3", url2);
try {
    strippedUrl = f.stripUrl('http' + url2);
    assert.equal(expectedUrl, strippedUrl);
    strippedUrl = f.stripUrl('https' + url2);
    assert.equal(expectedUrl, strippedUrl);
    console.log("Passed");
}
catch(assertionError) {
    console.log("FAILED with", assertionError.message);
}

/*******************************************************
 * Test remapUrl Wikipedia mobile link with anchor
 ******************************************************/
let urlRu1 = 'https://en.m.wikipedia.org/wiki/Nikola_Tesla#Tesla_coil';
strippedUrl = f.remapUrl(urlRu1);
expectedUrl = 'https://en.wikipedia.org/wiki/Nikola_Tesla';
console.log("remapUrl() Test 1", urlRu1);
try {
    assert.equal(expectedUrl, strippedUrl);
    console.log("Passed");
}
catch(assertionError) {
    console.log("FAILED with", assertionError.message);
}

/*******************************************************
 * Test remapUrl Wikipedia mobile link
 ******************************************************/
let urlRu2 = 'https://en.m.wikipedia.org/wiki/Nikola_Tesla';
strippedUrl = f.remapUrl(urlRu2);
expectedUrl = 'https://en.wikipedia.org/wiki/Nikola_Tesla';
console.log("remapUrl() Test 2", urlRu2);
try {
    assert.equal(expectedUrl, strippedUrl);
    console.log("Passed");
}
catch(assertionError) {
    console.log("FAILED with", assertionError.message);
}

/*******************************************************
 * Test remapUrl Wikipedia link with anchor
 ******************************************************/
let urlRu3 = 'https://en.wikipedia.org/wiki/Nikola_Tesla#Tesla_coil';
strippedUrl = f.remapUrl(urlRu3);
expectedUrl = 'https://en.wikipedia.org/wiki/Nikola_Tesla';
console.log("remapUrl() Test 3", urlRu3);
try {
    assert.equal(expectedUrl, strippedUrl);
    console.log("Passed");
}
catch(assertionError) {
    console.log("FAILED with", assertionError.message);
}

let result, expectedResult;;

/*******************************************************
 * Test shouldIgnore 1
 ******************************************************/
let urlIgnore1 = 'https://watermark.silverchair.com/jiq016.pdf';
result = f.shouldIgnore(urlIgnore1);
expectedResult = "true: file extension";
console.log("urlIgnore() Test 1", urlIgnore1);
try {
    assert.equal(expectedResult, result);
    console.log("Passed");
}
catch(assertionError) {
    console.log("FAILED with", assertionError.message);
}

/*******************************************************
 * Test shouldIgnore
 ******************************************************/
let urlIgnore2 = 'https://watermark.silverchair.com/jiq016.pdf?token=AQECAHi208BE49Ooanu';
result = f.shouldIgnore(urlIgnore2);
expectedResult = 'true: file extension';
console.log("urlIgnore() Test 2", urlIgnore2);
try {
    assert.equal(expectedResult, result);
    console.log("Passed");
}
catch(assertionError) {
    console.log("FAILED with", assertionError.message);
}
