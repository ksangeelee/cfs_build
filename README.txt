This project has been abandoned due to Google's intention to deprecate the
WebRequest API in Google Chrome.  The purpose of the project was to survey a
corpus of URL links from high quality sources (in this case Hacker News) and,
for each, to store the number of blocked ad/tracking requests (as determined by
uBlock Origin), along with the main text content of the URL.

This is used to build a search engine based on Lucene which allows you to
filter results not only on search keywords, but also on the number of uBlock
hits.

The code that will replace this is based on Selenium, driving Firefox, with a
modified version of uBlock Origin that posts the survey results (e.g. the
number of hits) to an HTTP server for storage and subsequent indexing.

It transpires that this is a better approach, because the level of detail
available in the survey output is higher. Also, while Puppeteer gives tighter
browser integration than Selenium (because it's tightly bound to Chrome,
whereas Selenium tries to abstract the interface to support multiple browsers),
my implementation depended on specific behaviour of Puppeteer and Chrome, and
so was dependent of the whims of Google.

Beyond this, the new implementation now breaks the process into more logical
responsibilities so that the effort can be more easily spread across many small
VPS servers.

The new code will be released when installation can be automated; right now
it's still bound to my dev environment. Email me if you want a tarball in the
meantime.
