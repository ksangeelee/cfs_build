const mongodb =     require('mongodb');
const mongoClient = mongodb.MongoClient;
const Binary =      mongodb.Binary;
const urlfilter =   require('../urlfilter');

let mongo = {
    url: 'mongodb://localhost:27017',
    databaseName: 'commerce_filter',
    client: undefined,
    database: undefined
}

let response_collection = 'url_response';
let duplicate_collection = 'url_duplicates'

async function initMongo(config) {

    config.client = await mongoClient.connect(config.url, {useNewUrlParser:true});
    config.database = mongo.client.db(config.databaseName);
}

function closeMongo() {

    mongo.client.close();    
}

/*
 * Function urlExists(url) checks if the given URL has already been processed.
 */
async function urlExists(url) {

    // Check for the document by url or meta.url
    const col = mongo.database.collection(response_collection);
    
    const hasUrl = await col.find(
        {
            $or: [ { "url": url }, { "meta.url": url } ]
        },
        {
            limit: 1
        }).hasNext();

    return hasUrl;
}

async function removeDiscardable() {

    let col = mongo.database.collection(response_collection);

    await col.find({}, { }).forEach( (doc) => {
        let urlInfo = urlfilter.getUrlInfo(doc.url);
        if(urlInfo.shouldDiscard) {
            console.log(doc.url);
            console.log(urlInfo);
            col.deleteOne({"_id": doc._id});
        }
    });
}

async function addMd5Urls() {

    let col = mongo.database.collection(response_collection);

    await col.find({}, { }).forEach( (doc) => {

        if(doc.urlMd5)
            return;

        let urlInfo = urlfilter.getUrlInfo(doc.url);
        let metaUrlInfo = urlfilter.getUrlInfo(doc.meta.url);

        if(urlInfo.strippedUrl == metaUrlInfo.strippedUrl) {
            doc.urlMd5 = urlInfo.md5Url;
            delete doc.meta.url;
        } else {
            doc.urlMd5 = urlInfo.md5Url;
            doc.meta.urlMd5 = metaUrlInfo.md5Url;
            delete doc.meta.url;
        }
        col.replaceOne( {"_id": doc._id}, doc );
    });
}

async function enforceBinaryMd5Urls() {

    let col = mongo.database.collection(response_collection);

    await col.find({}, { }).forEach( (doc) => {

        if("string" == typeof doc.urlMd5) {
            let binMd5 = Buffer.from(doc.urlMd5, 'hex');
            doc.urlMd5 = new Binary(binMd5, Binary.SUBTYPE_MD5);
        }

        if("string" == typeof doc.meta.urlMd5) {
            let binMd5 = Buffer.from(doc.meta.urlMd5, 'hex');
            doc.urlMd5 = new Binary(binMd5, Binary.SUBTYPE_MD5);
        }
        col.replaceOne( {"_id": doc._id}, doc );
    });
}

/**
 * Some tests to show what does and doesn't work when comparing
 * Buffers in BSON Binary objects.
 */
async function testMd5BinaryBuffers() {

    let col = mongo.database.collection(response_collection);

    // Assume we have a string MD5 that we want to find
    let md5Str = "aa9da60456e61f3f8a9f82ba11e91dbf";

    // First we make a byte Buffer from the hex-encoded string.
    let binMd5 = Buffer.from(md5Str, 'hex');

    // Now we create a BSON Binary object with out Buffer.
    let targetUrlMd5 = new Binary(binMd5, Binary.SUBTYPE_MD5);

    let bin1 = [], bin2 = [];

    // We can finally use the Binary object in a query.
    await col.find( { }, {limit: 3 }).forEach( (doc) => {
      bin1.push(doc.urlMd5);
    });
    await col.find( { }, {limit: 3 }).forEach( (doc) => {
      bin2.push(doc.urlMd5);
    });

    console.log(bin1);
    console.log(bin2);

    console.log("bin1[0] == bin2[0] ?", (bin1[0] == bin2[0]));
    console.log("Object.is(bin1[0], bin2[0]) ?", Object.is( bin1[0], bin2[0]));
    console.log("bin1[0].value() == bin2[0].value() ?",bin1[0].value() == bin2[0].value());
    console.log("Buffer.compare(bin1[0].buffer, bin2[0].buffer) ?",
                    Buffer.compare(bin1[0].buffer, bin2[0].buffer));
    console.log("bin1[0].buffer.equals(bin2[0].buffer)",bin1[0].buffer.equals(bin2[0].buffer));
}

async function checkBinaryMd5Urls() {

    let col = mongo.database.collection(response_collection);

    // Assume we have a string MD5 that we want to find
    let md5Str = "aa9da60456e61f3f8a9f82ba11e91dbf";

    // First we make a byte Buffer from the hex-encoded string.
    let binMd5 = Buffer.from(md5Str, 'hex');

    // Now we create a BSON Binary object with out Buffer.
    let targetUrlMd5 = new Binary(binMd5, Binary.SUBTYPE_MD5);

    // We can finally use the Binary object in a query.
    await col.find( { urlMd5: targetUrlMd5 }, {limit: 3 }).forEach( (doc) => {

      console.log(doc.urlMd5);
      /* The output object is
       
       Binary {
        _bsontype: 'Binary',
          sub_type: 5,
          position: 16,
          buffer: <Buffer aa 9d a6 04 56 e6 1f 3f 8a 9f 82 ba 11 e9 1d bf> }
      */
    });
}

async function run() {

    await initMongo(mongo).catch( () => {
        throw "Failed to create the mongo client.";
    });

    // await removeDiscardable();
    // await addMd5Urls();
    // await enforceBinaryMd5Urls();
    // await checkBinaryMd5Urls();
    await testMd5BinaryBuffers();

    closeMongo();
}

/**
 * Ok, let's go...
 */

console.log("Start processing: invoking run()");

run()
.then( () => console.log("Done") )
.catch(err => {
    console.log("Error", err);
});


