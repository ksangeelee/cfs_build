module.exports = {
    extractHrefs, 
    dereferenceRedirectors
};

var http = require('http');
var https = require('https');

const Entities = require('html-entities').AllHtmlEntities;
const entities = new Entities();

/*
 * function dereferenceRedirectors
 *
 * Given a URL, fetch the headers and extract any Location: URL
 */

function dereferenceRedirectors(url) {

    let matches = url.match(/^(https?):\/\/(bitly\.com|bit\.ly|goo\.gl)\//);

    if(matches == null) {
    	return Promise.resolve(url);
    }

    let isHttps = ( matches[1] == 'https' );

    return new Promise( (resolve, reject) => {

        let f = async (res) => {
            const location = res.headers['location'];
            if(location != undefined && location.length > 8) {
                resolve(res.headers['location']);
            } else {
                reject("URL matched as redirector, but failed to dereference.");
            }
        };

        isHttps ? https.get(url, f) : http.get(url, f);
    });
}

/*
 * function extractHrefs(html)
 *
 * Given a string of HTML, extract all href attributes and return them as
 * an array of URLs.
 */
async function extractHrefs(html) {

    var regex = /href\s*=\s*(['"])(https?:\/\/[^ ]+?)[`]?\1/ig;
    var links = [];
    var link;

    while ((link = regex.exec(entities.decode(html))) !== null) {
        // The second match group is the URL
        await dereferenceRedirectors(link[2])
        .then( (url) => { 
            // Push the URL
            links.push(url); 
	    } )
        .catch( (err) => {
            console.error("Failed while dereferencing", link[2], err);
        }); 
    }
    return links;
}


